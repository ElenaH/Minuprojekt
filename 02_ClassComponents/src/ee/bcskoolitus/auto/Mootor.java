package ee.bcskoolitus.auto;

public class Mootor {
	private int voimsus;
	private KytuseTyyp kytuseTyyp;
	

	public int getVoimsus()
	{
		return this.voimsus; }
	
	public Mootor setVoimsus(int voimsus)
	{
		
		this.voimsus=voimsus;
		return this; }
	

	public KytuseTyyp getKytuseTyyp()
	{
		return this.kytuseTyyp; }
	
	public Mootor setKytuseTyyp(KytuseTyyp kytuseTyyp)
	{
		this.kytuseTyyp=kytuseTyyp;
	     return this;	
	}
	
	@Override
	public String toString () {
		
		return"Mootor[voimsus = "+voimsus+", kytuseTyyp = "+kytuseTyyp+"]";
	}
	
}
