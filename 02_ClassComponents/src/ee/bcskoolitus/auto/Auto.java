package ee.bcskoolitus.auto;

public class Auto {

	
	private int usteArv;
	private int kohtadeArv;
	private Mootor mootor;
	private String voti;
	private int id;
	private static int idCounter =0;
	
	
	public int getUsteArv()
	{
		return(usteArv);
	}
	
	public Auto setUsteArv(int usteArv)
	
	{
		this.usteArv=usteArv;
		
		return this;
	}
	
	
	public int getKohtadeArv()
	{
		return(this.kohtadeArv);
	}
	
	public Auto setKohtadeArv(int kohtadeArv)
	
	{
		this.kohtadeArv=kohtadeArv;
		return this;
	}
	
	
	public Mootor getMootor()
	{
		return(this.mootor);
	}
	
	public Auto setMootor(Mootor mootor)
	
	{
		this.mootor=mootor;
		return this;
	}

	public Auto()
	  { 
		idCounter++;
		id=idCounter;
         this.voti="0000";

	  }
	
public Auto(Mootor mootor)
  { 
	this(); //calls Auto() without arguments;
	this.mootor=mootor;

  }

@Override

public String toString () {
	
	return"Auto[id"+id+""+this.mootor.toString()+", usteArv = "+this.usteArv+", kohtadeArv = "+this.kohtadeArv+"]";
}



public void kaivitaMootor() {
	
	if (mootor.getKytuseTyyp().equals(KytuseTyyp.DIISEL)) {
	 System.out.println("K�ivita eels��de");
		}
	
	System.out.println("K�ivita starter");
}

public void kaivitaMootor(String voti) {
	
if (voti.equals(this.voti))	{
	
    System.out.println("Pane v�ti s��telukku");
    kaivitaMootor();

}
else
{
	System.out.println("V�ti v�i auto on vale, proovi uuesti");}
}



public String getVoti() {
	System.out.println("Safety violation");
	return voti;
}


public Auto setVoti(String uusVoti) {
	if (this.voti.equals("0000"))
	{
	this.voti = uusVoti;
	}
	else
	{
		System.out.println("V�ti on juba olemas, vaheta vana v�ti");
	}
	return this;
}
	
public Auto setVoti(String vanaVoti, String uusVoti) {
	if (this.voti.equals(vanaVoti))
	{
	this.voti = uusVoti;
	}
	else
	{
		System.out.println("Vana v�ti ei sobi");
	}
	return this;
}


public int getID () {
	return id;
	
}


}




