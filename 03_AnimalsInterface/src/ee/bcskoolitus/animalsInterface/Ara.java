package ee.bcskoolitus.animalsInterface;

public class Ara implements AnimalsInterface, BirdInterface {

	@Override
	public void singing() {
		System.out.println("Shieks");
		
	}

	@Override
	public void moulting() {
		System.out.println("Looks nice again");
		
	}

	@Override
	public void moveAhead() {
		System.out.println("Flying high");
		
	}

	
}
