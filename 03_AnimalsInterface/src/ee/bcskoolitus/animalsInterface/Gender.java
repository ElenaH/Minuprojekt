package ee.bcskoolitus.animalsInterface;

public enum Gender {

	MALE, FEMALE, CHANGEABLE, UNKNOWN;
}
