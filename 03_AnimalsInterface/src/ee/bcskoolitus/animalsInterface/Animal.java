package ee.bcskoolitus.animalsInterface;


public abstract class Animal implements AnimalsInterface{
	
	private String speciesName;
	private Gender gender;
	
		
	public String getSpeciesName() {
		return speciesName;
	}
	public Animal setSpeciesName(String speciesName) {
		this.speciesName = speciesName;
		return this;
	}
	public Gender getGender() {
		return gender;
	}
	public Animal setGender(Gender gender) {
		this.gender = gender;
		return this;
	}
	
	
	
	

}
