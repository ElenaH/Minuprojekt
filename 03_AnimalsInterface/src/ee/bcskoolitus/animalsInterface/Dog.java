package ee.bcskoolitus.animalsInterface;

public class Dog extends Animal{

	private String sleepingPlace;
	
	
	@Override
	public void moveAhead() {
		System.out.println("Run");
		
	}


	public String getSleepingPlace() {
		return sleepingPlace;
	}


	public void setSleepingPlace(String sleepingPlace) {
		this.sleepingPlace = sleepingPlace;
	}

	
	
}
