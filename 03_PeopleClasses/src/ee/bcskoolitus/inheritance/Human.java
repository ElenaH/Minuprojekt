package ee.bcskoolitus.inheritance;

import java.util.ArrayList;
import java.util.List;

public class Human {

	private String firstName = "";
	private String lastName;
	private Gender gender = Gender.UNKNOWN;
	private int age = 0;

	public final static List<Human> allPeople = new ArrayList<Human>();
	public static int PeopleNumber;

	public Human(String lastName) {
		super();
		this.lastName = lastName;
		allPeople.add(this);
		PeopleNumber++;

	}

	public void removeHuman (Human deadHuman){
		PeopleNumber--;
		Human.allPeople.remove(deadHuman);
		
	}

	public int getAge() {
		return age;
	}

	public Human setAge(int age) {
		if (age > 0) {
			this.age = age;
		}
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public Human setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Human setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Gender getGender() {
		return gender;
	}

	public Human setGender(Gender gender) {
		this.gender = gender;
		return this;
	}

	@Override

	public String toString() {

		return ("Person named " + firstName + " " + lastName + ", aged " + age + "; gender=" + gender);

	}

}
