package ee.bcskoolitus.inheritance;

public enum Subject {

	MATHS, ENGLISH, SCIENCE, PE, ICT, UNKNOWN;
}
