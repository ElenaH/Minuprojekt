package ee.bcskoolitus.inheritance;

import java.util.HashSet;
import java.util.Set;

public class Teacher extends Human {
	
	private Set <Subject> subjects = new HashSet <Subject> ();
	
	
	
public Teacher (String lastName) {
		super(lastName);
		this.subjects.add(Subject.UNKNOWN);
		}
		
	
public Teacher (String lastName, Subject...subjects ) {
		super(lastName);
		for (Subject subjectFromInput: subjects ) {
			this.subjects.add(subjectFromInput);
		}
		
		
		if (this.subjects.size()>1) {
				this.subjects.remove(Subject.UNKNOWN);
					
}
}

		
/*private Teacher deleteUnknowns() {
	
	if (this.subjects.size()>1) {
		for (Subject subjectFromSet: subjects ) {
			if (subjectFromSet.equals(Subject.UNKNOWN)){
				this.subjects.remove(Subject.UNKNOWN);
			}
		};
		
		
	}
		
	return this;
}
*/
		//simple remove works too
		

@Override

public String toString() {

	return ("Teacher named " + getFirstName() + " " + getLastName() + ", aged " + getAge() + " teaches"+subjects+ "; gender=" + getGender());

}
		
	
	
	


}
