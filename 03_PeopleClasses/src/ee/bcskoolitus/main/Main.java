package ee.bcskoolitus.main;

import ee.bcskoolitus.inheritance.Gender;
import ee.bcskoolitus.inheritance.Human;
import ee.bcskoolitus.inheritance.Student;
import ee.bcskoolitus.inheritance.Subject;
import ee.bcskoolitus.inheritance.Teacher;

public class Main {

	public static void main(String[] args) {


		Human simpleHuman = new Human("Smith");
		
		simpleHuman.setFirstName("John");
		
		Student goodStudent = new Student("Gold");
		
		goodStudent.setAge(32).setGender(Gender.FEMALE);
		
		Teacher coolTeacher = new Teacher("Johnson", Subject.SCIENCE, Subject.ENGLISH, Subject.UNKNOWN);
		
		
		for(Human person: Human. allPeople) {
			
			System.out.println(person);
		}
		
		
		//Human studentMari = new Student("Mari"); //can do like this, but the methods for Student are not visible unless you change the type
		//((Student)studentMari).getGrade(); //like this it can access students's fields
		
		
	//	Human studentMati = new Human("Mari"); 
	//	((Student)studentMati).getGrade(); //returns error on compilation
		
		// Student studentHenry =new Human("Henry");//doesn't allow
		
		
		//with instanceof you can check if it's of this type or not, if it's a parent type, still says "yes"
	//	if(studentMari instanceof Student) {       
	//		System.out.println("on �pilane");
	 //    	}else {
	     		
	//     		System.out.println("ei ole �pilane");
	 //    	}
			
			
		//Human test =null;
		// (test instanceof Human) gives "false"
		
		
	}

}
