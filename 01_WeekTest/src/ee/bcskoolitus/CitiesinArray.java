package ee.bcskoolitus;

import java.util.Arrays;

public class CitiesinArray {

	final static int ENGLISH_CITYNAME = 0;
	final static int ESTONIAN_CITYNAME = 1;
	final static int LOCAL_CITYNAME = 2;
	final static int ESTONIAN_COUNTRYNAME = 3;

	public static void main(String[] args) {

		final String[] FRANCE = { "Paris", "Pariis", "Paris", "Prantsusmaa" };
		final String[] RUSSIA = { "Moscow", "Moskva", "Москва", "Venemaa" };
		final String[] ENGLAND = { "London", "London", "London", "Inglismaa" };
		final String[] FINLAND = { "Helsinku", "Helsingi", "Helsinki", "Soome" };
		final String[] ITALY = { "Rome", "Rooma", "Roma", "Itaalia" };

		String[][] myCitiesinArray = new String[195][4];

		// 2
		makeCitiesInArray(myCitiesinArray);

		// 3
		addCityLine(RUSSIA, myCitiesinArray);
		addCityLine(FRANCE, myCitiesinArray);
		addCityLine(ENGLAND, myCitiesinArray);
		addCityLine(FINLAND, myCitiesinArray);
		addCityLine(ITALY, myCitiesinArray);

		// 4
		printEstonianNames(myCitiesinArray);

		// 5
		System.out.println("Ülesanne 5");
		printAllNames(myCitiesinArray);

		System.out.println("Ülesanne 9");
		final String[] KURRUNURRU = { "LongStocking City", "Pikksuka linn", "Langstrump",
				"Kurrunurruvutisaare Kuningriik" };

		insertCityLine(0, KURRUNURRU, myCitiesinArray);

		printAllNames(myCitiesinArray);

	}

	// methods makeCitiesInArray, AddCityLine, PrintEstoniannames

	public static void makeCitiesInArray(String[][] allCitiesinArray) {

		String[] currentLine = { "English_cityname", "Estonian_cityname", "Local_cityname", "Estonian_countryname" };
		allCitiesinArray[0] = currentLine;

	}

	public static void addCityLine(String[] cityLinetoAdd, String[][] allCitiesinArray) {
		int counter = 0;
		while (allCitiesinArray[counter][0] != null) {
			counter++;
		}
		allCitiesinArray[counter] = cityLinetoAdd;

	}

	public static void addCityLine(int StartPlace, String[] cityLinetoAdd, String[][] allCitiesinArray) {

		if (StartPlace < 0) {
			System.out.println("Error: invalid row number, please provide row number that is a natural number");

		} else if (allCitiesinArray[StartPlace][0] != null) {
			System.out.println("Error: this row is not empty, use method InsertCityLine");

		}

		else {

			allCitiesinArray[StartPlace] = cityLinetoAdd;
		}

	}

	public static void printEstonianNames(String[][] allCitiesinArray) {

		System.out.println("-----ülesanne 4-----");

		for (String[] current : allCitiesinArray) {

			if (current[0] != null) {
				System.out.println(current[ESTONIAN_CITYNAME]);
			}

		}

		/*
		 * //if there are no empty lines in between int counter=0; while (
		 * AllCitiesinArray[counter][0]!=null) {
		 * System.out.println(AllCitiesinArray[counter][ESTONIAN_CITYNAME]); counter++;
		 * }
		 */

	}

	public static void printAllNames(String[][] allCitiesinArray) {

	
		for (int rowNumber = 0; rowNumber < allCitiesinArray.length; rowNumber++) {

			if (allCitiesinArray[rowNumber][0] != null) {

				System.out.println("Riik - " + allCitiesinArray[rowNumber][ESTONIAN_COUNTRYNAME] + "; pealinn - "
						+ allCitiesinArray[rowNumber][ESTONIAN_CITYNAME] + "; inglise keeles - "
						+ allCitiesinArray[rowNumber][ENGLISH_CITYNAME] + "; kohalikus keeles - "
						+ allCitiesinArray[rowNumber][LOCAL_CITYNAME] + ".");

			}

		}

	}

	public static void insertCityLine(int startPlace, String[] newLine, String[][] allCitiesinArray)

	{
		String[] emptyEntry ={null, null, null, null};
		int counter = allCitiesinArray.length - 1;

			while (counter > startPlace-1) {
				
				 if (allCitiesinArray[counter][0]!=null)
				 {
								addCityLine(counter+1, allCitiesinArray[counter], allCitiesinArray);
								allCitiesinArray[counter]= emptyEntry;
							
				 }
				 
					counter--;
			}
			allCitiesinArray[startPlace]= emptyEntry;
			  addCityLine(startPlace, newLine, allCitiesinArray);
			
				

		

	}
	
	
	/*public static void recursiveInsertLastCityLine (int startPlace, String[][] allCitiesinArray)
	{
	
		
	}*/

}