package ee.bcskoolitus.salary;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Salary {

	private BigDecimal grossSalary = null;

	public BigDecimal getGrossSalary() {
		return grossSalary;
	}

	public Salary setGrossSalary(BigDecimal grossSalary) 
		
		throws IllegalArgumentException {
		
	     	if (grossSalary.compareTo(BigDecimal.ZERO)>0) {
		           this.grossSalary = grossSalary;
		           return this;
		     }
		    else {
		     
		         throw new IllegalArgumentException ();	
		     }
		}
	

	// BigDecimal.valueOf(0.8)

	// subtract add multiply



	public BigDecimal calculateNetSalary() {

		BigDecimal netSalary = null;
		final BigDecimal SALARY_500 = BigDecimal.valueOf(500);
		final BigDecimal SALARY_1000 = BigDecimal.valueOf(1000);
		final BigDecimal SALARY_2000 = BigDecimal.valueOf(2000);
		
		//if you put final BigDecimal(500); the numbers are too long.

		final BigDecimal TAXFREE_1000 = BigDecimal.valueOf(500);
		final BigDecimal TAXFREE_2000 = BigDecimal.valueOf(250);
		final BigDecimal TAXFREE_2001 = BigDecimal.valueOf(0);

		if (grossSalary.compareTo(SALARY_500) <= 0) {
			netSalary = this.grossSalary;
		} else if (grossSalary.compareTo(SALARY_1000) < 0) {
            netSalary=this.calculate(TAXFREE_1000);  
		} else if (grossSalary.compareTo(SALARY_2000)<0) {
			netSalary=this.calculate(TAXFREE_2000);
		} else {
			netSalary=this.calculate(TAXFREE_2001);
		}
		
		
		return netSalary.setScale(2,RoundingMode.HALF_UP);

	}

	public BigDecimal calculate(BigDecimal taxfree) {

		final BigDecimal TAX_MULTIPLIER = BigDecimal.valueOf(0.8);
		return grossSalary.subtract(taxfree).multiply(TAX_MULTIPLIER).add(taxfree);

	}
	
	
	public BigDecimal socialTax ()
	{
		return grossSalary.multiply(BigDecimal.valueOf(0.3)).setScale(2, RoundingMode.HALF_UP);
	}
	

	public void printSalary()
	{
	System.out.println("GrossSalary="+this.getGrossSalary()+";  NetSalary=" +this.calculateNetSalary()+";  Social Tax="+ this.socialTax());
	}
	
}
