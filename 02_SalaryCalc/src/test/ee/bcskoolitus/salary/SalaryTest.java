package test.ee.bcskoolitus.salary;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import ee.bcskoolitus.salary.Salary;

public class SalaryTest {

@Test
	//


	public void testSocialTax () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(1000);
	BigDecimal expectedSocialTax=BigDecimal.valueOf(300.0);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedSocialTax.setScale(2), testSalary.socialTax());
	
}

@Test (expected = IllegalArgumentException.class)

public void testSetNegativeGrossSalary () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(-1000);
	Salary testSalary = new Salary ();
	testSalary.setGrossSalary (givenGrossSalary);
	
}




@Test (expected = IllegalArgumentException.class)

public void testSetZeroGrossSalary () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(0);
	Salary testSalary = new Salary ();
	testSalary.setGrossSalary (givenGrossSalary);
	
}



@Test


public void testCalculateSocialTaxWithCommaValue() {
	
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(777.87);
	BigDecimal expectedSocialTax=BigDecimal.valueOf(233.36);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedSocialTax, testSalary.socialTax());
	
	
}


@Test

public void testCalculateNetSalaryOver500 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(510);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(508);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}

// 498, 500, 510, 999, 1000, 1001, 1999, 2000, 2001
@Test

public void testCalculateNetSalaryUnder500 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(498);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(498);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}



@Test

public void testCalculateNetSalaryUnder1000 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(900);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(820);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}


@Test
public void testCalculateNetSalary1000 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(1000);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(850);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}


@Test
public void testCalculateNetSalaryUnder2000 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(1990);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(1642);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}


@Test
public void testCalculateNetSalary2000 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(2000);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(1600);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}



@Test
public void testCalculateNetSalaryOver2000 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(2100);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(1680);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}

@Test
public void testCalculateNetSalary1 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(0.50);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(0.50);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}

@Test
public void testCalculateNetSalary999 () {
	
	BigDecimal givenGrossSalary =BigDecimal.valueOf(999);
	
	BigDecimal expectedNetSalary=BigDecimal.valueOf(899.2);

	Salary testSalary = new Salary();
	testSalary.setGrossSalary(givenGrossSalary);
	
	Assert.assertEquals(expectedNetSalary.setScale(2), testSalary.calculateNetSalary());
	
	
}

}