package ee.bcskoolitus;

public class NumbridPildid {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
          
		//lisa3
		
		drawCharTriangle (6, '#');
		//lisa4
		
		drawReversedTriangle (6, '@');
		
		//lisa5
		
		int myNumber=34781;
		
		System.out.println("\n Reversed number for "+ myNumber+ " is " + reverseNumber(myNumber));
		
		
		
	}

	
	
	
	public static void drawCharTriangle (int longestSide, char drawingChar)
	
	{
		for (int row=0; row<longestSide; row++)
		{
			for (int SymbolQuantity=longestSide-row; SymbolQuantity>0; SymbolQuantity--)
				{
				  System.out.print(drawingChar);
				}
			
			System.out.println();
	
	    }
	
     }
	

	public static void drawReversedTriangle (int longestSide, char drawingChar)
	
	{
		for (int row=0; row<longestSide; row++)
		{
			for (int SpaceQuantity=longestSide-row; SpaceQuantity>0; SpaceQuantity--)
				{
				  System.out.print(' ');
				};
						
								
			for (int SymbolQuantity=1; SymbolQuantity<row+1; SymbolQuantity++)
			{
			  System.out.print(drawingChar);
			};
		
			
			System.out.println();
	
	    }
	
     }
	
	public static int reverseNumber(int NumbertoReverse)
	
	{ 
		int newNumber=0;
		
		while (NumbertoReverse>0)
		{
			newNumber=newNumber*10+NumbertoReverse % 10;
			
			NumbertoReverse=NumbertoReverse/10;
			
		};
			
		
			return(newNumber);
	}
	
	
}
